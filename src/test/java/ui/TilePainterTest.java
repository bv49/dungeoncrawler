package ui;

import java.awt.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import engine.GameEngine;
import tiles.TileType;
import values.TileColorMap;

public class TilePainterTest {

	private final int TILE_WIDTH = 10;
	private final int TILE_HEIGHT = 20;
	private final int X = 2;
	private final int Y = 3;

	@Mock
	Graphics graphics;
	@Mock
	GameEngine gameEngine;
	@InjectMocks
	TilePainter tilePainter;



	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void paint_tiles() {
		Mockito.when(gameEngine.getLevelHorizontalDimension()).thenReturn(X);
		Mockito.when(gameEngine.getLevelVerticalDimension()).thenReturn(Y);
		Mockito.when(gameEngine.getTileFromCoordinates(1, 1)).thenReturn(TileType.NOT_PASSABLE);
		Mockito.when(gameEngine.getTileFromCoordinates(AdditionalMatchers.not(ArgumentMatchers.eq(1)),
				AdditionalMatchers.not(ArgumentMatchers.eq(1)))).thenReturn(TileType.PASSABLE);

		tilePainter.paintTiles(graphics, gameEngine, TILE_WIDTH, TILE_HEIGHT);

		InOrder inOrder = Mockito.inOrder(graphics);
		inOrder.verify(graphics).setColor(TileColorMap.get(TileType.PASSABLE));
		inOrder.verify(graphics).fillRect(0, 0, 10, 20);
		inOrder.verify(graphics).fillRect(0, 20, 10, 20);
		inOrder.verify(graphics).fillRect(0, 40, 10, 20);
		inOrder.verify(graphics).fillRect(10, 0, 10, 20);
		inOrder.verify(graphics).setColor(TileColorMap.get(TileType.NOT_PASSABLE));
		inOrder.verify(graphics).fillRect(10, 20, 10, 20);
		inOrder.verify(graphics).fillRect(10, 40, 10, 20);

	}

	@Test
	public void paint_player() {

		tilePainter.paintPlayer(graphics, X, Y, TILE_WIDTH, TILE_HEIGHT, TileType.PLAYER);

		Mockito.verify(graphics).fillRect(20, 60, 10, 20);
	}

}
